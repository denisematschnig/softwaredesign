package pizza;

public class Margarita extends Pizza
{
	@Override
	protected void topping()
	{
		System.out.println("Putting cheese on the pizza...");
		System.out.println("Putting more cheese on the pizza...");
	}

	@Override
	public String getName()
	{
		return "Margarita";
	}

}
