package pizza;

public class Calzone extends Pizza
{
	@Override
	public void bake()
	{
		System.out.println("Der Teig wird zusammengerollt");
		System.out.println("Die Pizza wird gebacken...\n");
	}

	@Override
	protected void topping()
	{
		System.out.println("Putting cheese on the pizza...");
		System.out.println("Putting more cheese on the pizza...");
	}

	@Override
	public String getName()
	{
		return "Calzone";
	}

}
