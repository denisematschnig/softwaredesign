package pizza;

public class Pizzeria
{
	public iPizza order(String type)
	{
		iPizza p = createPizza(type);
		p.make();

		return p;
	}

	public Pizza createPizza(String type)
	{
		if (type.equalsIgnoreCase("Margarita"))
			{
				return new Margarita();
			}
		if (type.equalsIgnoreCase("Calzone"))
			{
				return new Calzone();
			}
		return null;
	}

}
