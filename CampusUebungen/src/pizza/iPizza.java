package pizza;

public interface iPizza
{
	String getName();

	void make();

	default void bake()
	{
		System.out.println("Die Pizza wird gebacken...\n");
	}
}
