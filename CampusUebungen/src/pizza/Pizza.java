package pizza;

public abstract class Pizza implements iPizza
{
	protected abstract void topping();

	@Override
	public abstract String getName();

	@Override
	public void make()
	{
		System.out.println("Zubereitung einer " + getName() + " \n");
		dough();
		topping();
		bake();
	}

	protected static void dough()
	{
		System.out.println("Der Teig wird zubereitet...");
	}

}
