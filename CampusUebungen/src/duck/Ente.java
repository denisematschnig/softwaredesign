package duck;

public class Ente
{
	protected FlugVerhalten flugverhalten;

	public Ente()
	{
		flugverhalten = new ElegantesFlugverhalten();
	}

	public void fliegen()
	{
		flugverhalten.fliegen();
	}
}
