package berverage;

public class Coffee extends Beverage
{
	@Override
	public String getName()
	{
		return "Coffee";
	}

	@Override
	public void addIngredients()
	{
		System.out.println("Grind the coffee beans...");
		System.out.println("Make the coffee...");
	}

}
