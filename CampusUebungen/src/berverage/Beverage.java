package berverage;

public abstract class Beverage 
//implements iBeverage
{
	public abstract String getName();
	protected abstract void addIngredients();

	public void make()
	{
		boilWater();
		pourWaterInCup();
		addIngredients();
		serveBeverage();
	}
	
	protected static void boilWater()
	{
		System.out.println("Waiting till water is boiling...");
	}
	
	protected static void pourWaterInCup()
	{
		System.out.println("Schhhhhhhhhhhhh");
	}
	
	protected static void serveBeverage()
	{
		System.out.println("Caution hot...!");
	}
	
}
