package berverage;

public class Tea extends Beverage
{
	@Override
	public String getName()
	{
		return "Tea";
	}

	@Override
	public void addIngredients()
	{
		System.out.println("But teabag into the water...");

	}

}
