package berverage;

public class Main
{

	public static void main(String[] args)
	{
		Beverage coffee = new Coffee();
		Beverage tea = new Tea();
		
		coffee.make();
		System.out.println();
		tea.make();
		System.out.println();
		orderBeverage(new Coffee());

	}
	
	public static void orderBeverage(Beverage b) {
		b.make();
	}

}
