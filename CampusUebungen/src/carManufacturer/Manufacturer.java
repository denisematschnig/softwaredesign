package carManufacturer;

public abstract class Manufacturer
{
	public void orderCar(String brand)
	{
		CarType cT = manufactureCar(brand);
	}

	protected abstract CarType manufactureCar(String brand);
}
