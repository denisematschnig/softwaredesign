package carManufacturer;

public class LKW extends CarType
{

	@Override
	public String getName()
	{
		return "LWK";
	}

	@Override
	public void assembleVehicle()
	{
		System.out.println("The LWK is being assembled...");
		
	}

}
