package carManufacturer;

public class PKW extends CarType
{

	@Override
	public String getName()
	{
		return "PKW";
	}

	@Override
	public void assembleVehicle()
	{
		System.out.println("The PKW is being assembled...");
		
	}

}
