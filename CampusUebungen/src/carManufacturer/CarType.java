package carManufacturer;

public abstract class CarType implements iCarType
{
	public abstract String getName();

	public abstract void assembleVehicle();

	public void manufacture(CarType cT)
	{
		getParts(cT);
		assembleVehicle();
		cleanVehicle(cT);
		shipVehicle(cT);
	}

	protected static void getParts(CarType cT)
	{
		System.out.println("Getting parts for the " + cT + "...");
	}

	protected static void shipVehicle(CarType cT)
	{
		System.out.println("The " + cT + " is shipped to the customer...");
	}

	protected static void cleanVehicle(CarType cT) {
		System.out.println("The " + cT + " is being cleaned...");
	}
}
