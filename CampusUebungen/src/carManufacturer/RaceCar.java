package carManufacturer;

public class RaceCar extends CarType
{

	@Override
	public String getName()
	{
		return "Sportwagen";
	}

	@Override
	public void assembleVehicle()
	{
		System.out.println("The Racecar is being assembled...");
		
	}

}
