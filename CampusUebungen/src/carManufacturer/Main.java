package carManufacturer;

public class Main
{

	public static void main(String[] args)
	{
//		Manufacturer manu = new Manufacturer();
//		
//		iCarType pkw = manu.manufactureCar("pkw");
//		System.out.println();
//		iCarType lkw = manu.manufactureCar("lkw");
//		System.out.println();
//		iCarType racer = manu.manufactureCar("sportwagen");
		
		System.out.println("----------Germany----------");
		Manufacturer de = new GermanManufacturer();
		de.orderCar("BMW");
		
		
		System.out.println("\n----------GreatBritain----------");
		Manufacturer gb = new GreatBritainManufacturer();
		gb.orderCar("RollsRoyce");
		
	}

}
