package canon;

public class Calculator
{

	public int calc(int a, int b, Rechenart rechenart)
	{
		System.out.println("Berechnung mit " + a + " & " + b);
		return rechenart.calc(a, b);
	}

}
