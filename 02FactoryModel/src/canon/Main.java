package canon;

public class Main
{

	public static void main(String[] args)
	{
		Calculator calculator = new Calculator();
		

		int result = calculator.calc(1, 2, new Add());
		System.out.println("Add: " + result);

		result = calculator.calc(1, 2, new Substract());
		System.out.println("Sub: " + result);

		result = calculator.calc(1, 2, new Multiply());
		System.out.println("Mul: " + result);

		result = calculator.calc(1, 2, new Divide());
		System.out.println("Div:" + result);
	}

}
