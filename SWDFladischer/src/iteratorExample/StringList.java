package iteratorExample;

import java.util.Iterator;

public class StringList implements Iterable<String>
{
	private String[] text;
	
	
	public StringList(String text)
	{
		super();
		this.text = text.split(" ");
	}

	@Override
	public Iterator<String> iterator()
	{
		return new StringListIterator(text);
	}

}
