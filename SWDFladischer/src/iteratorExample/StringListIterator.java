package iteratorExample;

import java.util.Iterator;

public class StringListIterator implements Iterator<String>
{
	private String[] words;
	private int position;
	
	public StringListIterator(String[]words)
	{
		super();
		this.words = words;
		this.position = words.length - 1;
	}

	@Override
	public boolean hasNext()
	{	
		return position >= 0;
	}

	@Override
	public String next()
	{
		return words[position--];
	}
	


}
