package commandPattern;

public class TV
{
	private boolean isOn = false;
	private int channel = 1;

	public void on()
	{
		isOn = true;
		System.out.println("Fernseher ist ein auf channel " + channel);
	}

	public void off()
	{
		isOn = false;
		System.out.println("Fernseher ist aus");
	}

	public void channelUp()
	{
		if(isOn) {
			channel++;
			System.out.println("Neuer channel " + channel);
		}
		else {
			return;
		}
	}

	public void channelDown()
	{
		if(isOn) {
			channel--;
			System.out.println("Neuer channel " + channel);
		}
		else {
			return;
		}
	}
}
