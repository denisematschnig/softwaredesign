package commandPattern;

public class LightToggleCommand implements iCommand
{

	private Light light;

	public LightToggleCommand(Light light)
	{
		super();
		this.light = light;
	}

	@Override
	public void execute()
	{
		if (light.isOn())
		{
			light.off();
		} else {
			light.on();
		}
	}

}
