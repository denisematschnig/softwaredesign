package commandPattern;

public interface iCommand
{
	public void execute();
}
