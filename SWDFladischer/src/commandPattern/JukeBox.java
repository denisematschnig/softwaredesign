package commandPattern;

public class JukeBox
{
	private boolean isOn = false;

	public void on()
	{
		isOn = true;
		System.out.println("The Jukebox is now on");
	}

	public void off()
	{
		isOn = false;
		System.out.println("The Jukebox is now off");
	}
}
