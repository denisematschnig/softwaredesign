package commandPattern;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class RemoteControl
{
	public static void main(String[] args)
	{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		Map<Integer, iCommand> buttons = new HashMap<Integer, iCommand>();

		Light l1 = new Light();
		Light l2 = new Light();

		TV tv = new TV();

		JukeBox jb = new JukeBox();

		buttons.put(1, new LightToggleCommand(l1));
		buttons.put(2, new LightToggleCommand(l2));
//		l1.on();
//		l1.dim(60);
//		jb.on();
//		tv.channelUp();
//		tv.channelUp();
//		tv.channelUp();
//		tv.on();
//		jb.off();
//		l2.on();
//		l1.off();
//		tv.channelDown();

		buttons.get(1).execute();
		buttons.get(2).execute();
		buttons.get(1).execute();
		buttons.get(2).execute();

		try
			{
				String br = reader.readLine();
				
				
				
				
			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	}
}
