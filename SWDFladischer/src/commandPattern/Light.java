package commandPattern;

public class Light
{
	private int dimFactor = 0;

	public void on()
	{
		dimFactor = 100;
		System.out.println("The light shines with " + dimFactor);
	}

	public void off()
	{
		dimFactor = 0;
		System.out.println("The light is now off");
	}

	public void dim(int dimFactor)
	{
		if(dimFactor < 0 || dimFactor > 100) {
			if(dimFactor < 0)
				System.out.println("The light is already out");
			if(dimFactor > 100)
				System.out.println("The light is already at 100%.");
			return;
		}
		System.out.println("Leuchte mit " + dimFactor + "%");
	}
	
	public boolean isOn() {
		return dimFactor > 0;
	}

}
