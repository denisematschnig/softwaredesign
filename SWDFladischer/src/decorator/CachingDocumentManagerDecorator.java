package decorator;

import java.io.*;
import java.util.*;

public class CachingDocumentManagerDecorator extends AbstractDocumentManagerDecorator
{
	// wenn einer erzeugt wird reicht der den manager an die superklasse weiter ==
	// abstractdocumentmanagerdecorator
	// und speichert es in der protected variable

	Map<String, String> cache = new HashMap<String, String>();

	public CachingDocumentManagerDecorator(iDocumentManager dm)
	{
		super(dm);
	}

	@Override
	public String load(String fileName) throws IOException
	{
		if (cache.containsKey(fileName))
			{
				System.out.println("Aus cache " + fileName);
				return cache.get(fileName);
			}

		System.out.println("Von HDD " + fileName);
		String content = super.load(fileName);
		cache.put(fileName, content);

		return content;
	}

	@Override
	public void saveOrUpdate(String fileName, String document) throws IOException
	{
		// zuerst schaun dass das ding sicher auf der platte ist (im schlimmsten fall
		// ist dann der cache falsch)
		super.saveOrUpdate(fileName, document);
		
		// wenn datei ge�ndert wurde werfen wir sie raus
		// beim n�chsen aufruf wird sie wieder im cache gespeichert
		// cache.remove(fileName);

		// datei wird nur geupdated
		cache.put(fileName, document);
	}
	
	public void flushCache() {
		cache.clear();
	}

}
