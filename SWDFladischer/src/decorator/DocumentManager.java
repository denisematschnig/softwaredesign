package decorator;

import java.io.*;

public class DocumentManager implements iDocumentManager
{

	@Override
	public String load(String fileName) throws IOException
	{
		BufferedReader inp = new BufferedReader(new FileReader(fileName));
		String content = "";
		String line;
		while((line = inp.readLine()) != null) {
			content += line;
		}
		return content;
	}

	@Override
	public void saveOrUpdate(String fileName, String document) throws IOException
	{
		Writer outp = new FileWriter(fileName);
		outp.write(document);
		outp.close();
	}

}
