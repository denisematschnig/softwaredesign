package decorator;

import java.io.IOException;

public interface iDocumentManager
{
	public String load(String fileName) throws IOException;

	public void saveOrUpdate(String fileName, String document) throws IOException;
}
