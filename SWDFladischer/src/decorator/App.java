package decorator;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Date;

public class App
{

	public static void main(String[] args) throws IOException
	{
		try(Writer logger = new FileWriter("logfile.txt"))
		{
		DocumentManager dm = new DocumentManager();
		CachingDocumentManagerDecorator idm = new CachingDocumentManagerDecorator(dm);
		AuditingDocumentManager adm = new AuditingDocumentManager(idm, logger);
		try
			{
//				System.out.println(idm.load("test.txt"));
//				idm.load("test.txt");
//				idm.load("test.txt");
//				idm.flushCache();
//				idm.load("test.txt");
//				idm.load("test.txt");
//				idm.load("test.txt");
//				idm.load("test.txt");
//				idm.load("test.txt");
//				idm.flushCache();
//				idm.load("test.txt");
//				idm.load("test.txt");
//				dm.saveOrUpdate("uhrzeit.txt", new Date().toString());	
				
				adm.load("test.txt");
				adm.load("test.txt");
				adm.load("test.txt");
				adm.load("test.txt");
				adm.load("test.txt");
				adm.load("test.txt");
				adm.load("test.txt");
				adm.saveOrUpdate("uhrzeit.txt", new Date().toString());
				
			} 
		
		catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}

}
