package decorator;

import java.io.*;
import java.util.Date;

public class AuditingDocumentManager extends AbstractDocumentManagerDecorator
{
	private Writer logger;

	public AuditingDocumentManager(iDocumentManager dm, Writer logger) throws IOException
	{
		super(dm);
		this.logger = logger;
	}
	
	@Override
	public String load(String fileName) throws IOException
	{
		logger.write("Datei geladen: " + fileName + " " + new Date().toString() + "\n");
		String content = super.load(fileName);
		logger.write("Datei geladen: " + fileName + " " + new Date().toString() + "\n");
		return content;
		
	}

	@Override
	public void saveOrUpdate(String fileName, String document) throws IOException
	{
		logger.write("Datei wird geschrieben " + fileName + " " + new Date().toString() + "\n");
		super.saveOrUpdate(fileName, document);
		logger.write("Datei wird geschrieben " + fileName + " " + new Date().toString() + "\n");
	}

}
