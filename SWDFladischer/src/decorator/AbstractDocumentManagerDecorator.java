package decorator;

import java.io.*;

public abstract class AbstractDocumentManagerDecorator implements iDocumentManager
{
	protected iDocumentManager dm;

	
	public AbstractDocumentManagerDecorator(iDocumentManager dm)
	{
		super();
		this.dm = dm;
	}

	@Override
	public String load(String fileName) throws IOException
	{
		return dm.load(fileName);

	}

	@Override
	public void saveOrUpdate(String fileName, String document) throws IOException
	{
		dm.saveOrUpdate(fileName, document);

	}
}
