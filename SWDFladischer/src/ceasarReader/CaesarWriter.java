package ceasarReader;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class CaesarWriter extends FileWriter
{
	private int key;

	public CaesarWriter(File arg0) throws IOException
	{
		super(arg0);
	}

	public void setKey(int key)
	{
		this.key = key;
	}
	
	@Override
	public void write(int pups) throws IOException
	{
		while(true) {
			super.write(pups + key);
		}
	}


}
