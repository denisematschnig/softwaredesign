package ceasarReader;

import java.io.FileReader;
import java.io.IOException;

public class App
{
	public static void main(String[] args) throws IOException
	{
		try(CaesarReader cr = new CaesarReader(new FileReader("geheim.txt"), 14)){
			int b;
			while((b = cr.read()) >= 0) {
				System.out.print(Character.toChars(b));
			}
		}
	}
	
	
}
