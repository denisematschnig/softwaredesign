package ceasarReader;

import java.io.FilterReader;
import java.io.IOException;
import java.io.Reader;

// This is the decorator for the filterReader
public class CaesarReader extends FilterReader
{
	private int key;

	public CaesarReader(Reader r, int key)
	{
		super(r);
		this.key = key;
	}

	@Override
	public int read() throws IOException
	{
		int c = super.read();
		if(c < 0) {
			return c;
		}
		return c - key;
	}
	
	
}
