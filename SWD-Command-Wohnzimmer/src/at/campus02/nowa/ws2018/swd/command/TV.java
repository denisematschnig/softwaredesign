package at.campus02.nowa.ws2018.swd.command;

public class TV {
	
	private boolean isOn = false;
	private int channel = 1;
	
	public void on() {
		isOn = true;
		System.out.println("Fernseher ist ein: " + channel);
	}
	
	public void off() {
		isOn = false;
		System.out.println("Fernseher ist aus");
	}
	
	public void up() {
		if (!isOn) {
			return;
		}
		channel++;
		System.out.println("Neuer Kanal: " + channel);
	}
	
	public void down() {
		if (!isOn) {
			return;
		}
		channel--;
		System.out.println("Neuer Kanal: " + channel);
	}

	public int getChannel() {
		return channel;
	}

	public void setChannel(int channel) {
		this.channel = channel;
		System.out.println("Neuer Kanal: " + channel);
	}

	public boolean isOn() {
		return isOn;
	}

}
