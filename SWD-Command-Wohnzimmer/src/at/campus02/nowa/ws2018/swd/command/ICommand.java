package at.campus02.nowa.ws2018.swd.command;

public interface ICommand {
	
	public void execute();
	public void undo();

}
