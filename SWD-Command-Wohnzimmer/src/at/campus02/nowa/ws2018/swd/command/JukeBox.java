package at.campus02.nowa.ws2018.swd.command;

public class JukeBox {
	
	private boolean isOn = false;
	
	public void on() {
		isOn = true;
		System.out.println("Jukebox ein");
	}
	
	public void off() {
		isOn = false;
		System.out.println("Jukebox aus");
	}
	
	public boolean isOn() {
		return isOn;
	}

}
