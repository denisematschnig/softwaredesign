package at.campus02.nowa.ws2018.swd.command;

public class TVChannelUpCommand implements ICommand {

	private TV tv;

	public TVChannelUpCommand(TV tv) {
		super();
		this.tv = tv;
	}

	@Override
	public void execute() {
		if (!tv.isOn()) {
			tv.on();
		} else {
			if (tv.getChannel() == 40) {
				tv.setChannel(1);
			} else {
				tv.up();
			}
		}
	}

	@Override
	public void undo()
	{
		Boolean isOn = history.pop();
		if(!isOn) {
			tv.off();
		} else {
			if(tv.getChannel() == 1) {
				tv.setChannel(40);
			}
		}
		
	}

}
