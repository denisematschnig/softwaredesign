package at.campus02.nowa.ws2018.swd.command;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

public class RemoteControl {

	public static void main(String[] args) {
		
		Map<Integer, ICommand> buttons = new HashMap<Integer, ICommand>();
		Stack<ICommand> history = new Stack<ICommand>();
		
		
		Light l1 = new Light();
		Light l2 = new Light();
		
		TV tv = new TV();
		
		JukeBox jb = new JukeBox();
		
		buttons.put(1, new LightToggleCommand(l1));
		buttons.put(2, new LightToggleCommand(l2));
		JukeBoxToggleCommand jbtc = new JukeBoxToggleCommand(jb);
		buttons.put(4, jbtc);
		buttons.put(6, jbtc);
		buttons.put(3, new TVChannelUpCommand(tv));
		buttons.put(5, new TVChannelDownCommand(tv));
		
		try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
			String line;
			while ((line = br.readLine()) != null) {
				try {
					Integer key = Integer.parseInt(line);
					if (buttons.containsKey(key)) {
						ICommand cmd = buttons.get(key);
						cmd.execute();
						history.add(cmd);
					}
				} catch (NumberFormatException e) {
					System.out.println("Keine g�ltige Taste!");
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	

	}

}
