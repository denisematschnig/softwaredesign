package at.campus02.nowa.ws2018.swd.command;

public class TVChannelDownCommand implements ICommand {
	
	private TV tv;
	
	public TVChannelDownCommand(TV tv) {
		super();
		this.tv = tv;
	}

	@Override
	public void execute() {
		if (!tv.isOn()) {
			tv.on();
		} else {
			if (tv.getChannel() == 1) {
				tv.setChannel(40);
			} else {
				tv.down();
			}
		}
	}

	@Override
	public void undo()
	{
		// TODO Auto-generated method stub
		
	}

}
