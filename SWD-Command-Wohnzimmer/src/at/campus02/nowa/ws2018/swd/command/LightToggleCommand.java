package at.campus02.nowa.ws2018.swd.command;

public class LightToggleCommand implements ICommand {
	
	private Light light;
	
	public LightToggleCommand(Light light) {
		super();
		this.light = light;
	}

	@Override
	public void execute() {
		if (light.isOn()) {
			light.off();
		} else {
			light.on();
		}
		
	}

	@Override
	public void undo()
	{
		// TODO Auto-generated method stub
		
	}

}
