package at.campus02.nowa.ws2018.swd.command;

public class JukeBoxToggleCommand implements ICommand {

	private JukeBox jb;

	public JukeBoxToggleCommand(JukeBox jb) {
		super();
		this.jb = jb;
	}

	@Override
	public void execute() {
		if (jb.isOn()) {
			jb.off();
		} else {
			jb.on();
		}

	}

	@Override
	public void undo()
	{
		// TODO Auto-generated method stub
		
	}

}
